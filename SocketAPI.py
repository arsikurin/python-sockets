import socket
from time import sleep


class Session:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def receive(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock1:
            sock1.bind((self.get_ip(), self.get_port()))

            print("[+] ClientReceive started")
            sleep(0.01)
            while True:
                data, addr = sock1.recvfrom(1024)
                data = data.decode("utf-8")

                if data == "close":
                    print(f"[!] Connection interrupted by Server")
                    exit()
                if data == "error":
                    print(
                        f"[!] Connection interrupted unexpectedly by Server")
                    exit("[!] Client is down")
                print(f"[data] {data}")

    def send(self, receiverServerIp, receiverServerPort):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock2:
            sock2.bind((self.get_ip(), self.get_port()))
            addr = (receiverServerIp, receiverServerPort)

            print("[+] ClientSend Started")
            sleep(0.01)
            while True:
                if (message := input()) == "":
                    print("[!] Cannot send null")
                    continue
                sock2.sendto(message.encode("utf-8"), addr)

    def get_ip(self):
        return self.ip

    def get_port(self):
        return self.port

    def show_info(self):
        print(f"ip = {self.ip}")
        print(f"port = {self.port}")
