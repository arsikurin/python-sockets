from SocketAPI import *
import socket
from threading import Thread

senderServer = Session("10.10.10.61", 4003)
receiverServer = Session("10.10.10.61", 4004)
receiverClient = Session("10.10.10.61", 4005)
senderClient = Session("10.10.10.61", 4006)
spareSender = Session("10.10.10.61", 4007)


def errorHandler(threadReceive):
    try:
        threadReceive.join()
    except KeyboardInterrupt:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock2:
            sock2.bind((spareSender.get_ip(), spareSender.get_port()))
            addr = (receiverClient.get_ip(), receiverClient.get_port())
            sock2.sendto("close".encode("utf-8"), addr)
        exit("[!] Connection interrupted by Server")
    except:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock2:
            sock2.bind((spareSender.get_ip(), spareSender.get_port()))
            addr = (receiverClient.get_ip(), receiverClient.get_port())
            sock2.sendto("error".encode("utf-8"), addr)
        exit("[!] Error occurred")


def main():
    threadSend = Thread(target=senderServer.send, args=(receiverClient.get_ip(), receiverClient.get_port()),
                        daemon=True)
    threadReceive = Thread(target=receiverServer.receive, daemon=True)
    print(f"ClientSend is {senderClient.get_ip()} {senderClient.get_port()}")
    print(f"ClientReceive is {receiverClient.get_ip()} {receiverClient.get_port()}")
    threadReceive.start()
    threadSend.start()
    errorHandler(threadReceive)


if __name__ == "__main__":
    main()
