from SocketAPI import *
import socket
from threading import Thread

senderServer = Session("10.10.10.61", 4003)
receiverServer = Session("10.10.10.61", 4004)
receiverClient = Session("10.10.10.61", 4005)
senderClient = Session("10.10.10.61", 4006)
spareSender = Session("10.10.10.61", 4007)


def errorHandler(threadReceive):
    try:
        threadReceive.join()
    except KeyboardInterrupt:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock2:
            sock2.bind((spareSender.get_ip(), spareSender.get_port()))
            addr = (receiverServer.get_ip(), receiverServer.get_port())
            sock2.sendto("close".encode("utf-8"), addr)
        exit("[!] Connection interrupted by Client")
    except:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock2:
            sock2.bind((spareSender.get_ip(), spareSender.get_port()))
            addr = (receiverServer.get_ip(), receiverServer.get_port())
            sock2.sendto("error".encode("utf-8"), addr)
        exit("[!] Error occurred")


def main():
    threadSend = Thread(target=senderClient.send, args=(receiverServer.get_ip(), receiverServer.get_port()),
                        daemon=True)
    threadReceive = Thread(target=receiverClient.receive, daemon=True)
    print(f"ServerSend is {senderServer.get_ip()} {senderServer.get_port()}")
    print(f"ServerReceive is {receiverServer.get_ip()} {receiverServer.get_port()}")
    threadReceive.start()
    threadSend.start()
    errorHandler(threadReceive)


if __name__ == "__main__":
    main()
